# video-streaming

**video-streaming** is a traffic generator that conists of two
components: an HTTP *server* which provides multiple video players,
and a browser *client* that automatically navigates to the server
and "watches" video.

While both the server and client components are designed to be used with
each other, each component can also be used independently.
The client will require specific modification to the website being
visited.

A peer-reviewed research paper describing the details of this video
streaming traffic generator can be found online:

> Calvin Ardi, Alefiya Hussain, and Stephen Schwab. 2021. **Building
> Reproducible Video Streaming Traffic Generators**. In *Cyber Security
> Experimentation and Test Workshop* (*CSET '21*). Association
> for Computing Machinery, New York, NY, USA, 91–95.
> DOI:`10.1145/3474718.3474721`.
> ([DOI](https://dl.acm.org/doi/10.1145/3474718.3474721),
> [PDF](https://dl.acm.org/doi/pdf/10.1145/3474718.3474721))

Additional information and a copy of this software and its source code
can also be found at <https://gitlab.com/mergetb/exptools/video-streaming>.

## quick start

The following instructions can be run on a Debian- or Ubuntu-based OS:
1. install GNOME, codecs, and Google Chrome browser:
   ```shell
   curl -L https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb > /tmp/google-chrome-stable_current_amd64.deb
   apt-get update
   echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections
   apt-get install -y ubuntu-gnome-desktop ubuntu-restricted-extras /tmp/google-chrome-stable_current_amd64.deb
   ```
2. install NodeJS to `/usr/local`:
   ```shell
   export NODEJS_VERSION="16.14.2"
   curl -L https://nodejs.org/dist/v${NODEJS_VERSION}/node-v${NODEJS_VERSION}-linux-x64.tar.xz > /tmp/node-v${NODEJS_VERSION}-linux-x64.tar.xz
   cd /usr/local
   sudo tar --strip-components 1 -xf /tmp/node-v${NODEJS_VERSION}-linux-x64.tar.xz
   ```

   test NodeJS:
   ```shell
   node --version
   ```
3. clone this repository:
   ```shell
   git clone https://gitlab.com/mergetb/exptools/video-streaming
   ```
4. client: install dependencies
   ```shell
   cd video-streaming/client
   npm install
   ```
5. server: download and install Caddy
   ```shell
   export CADDY_VERSION="2.4.6"
   curl -L https://github.com/caddyserver/caddy/releases/download/v${CADDY_VERSION}/caddy_${CADDY_VERSION}_linux_amd64.tar.gz > /tmp/caddy_${CADDY_VERSION}_linux_amd64.tar.gz
   cd /usr/local
   sudo tar -xf /tmp/caddy_${CADDY_VERSION}_linux_amd64.tar.gz
   ```

   test Caddy:
   ```shell
   caddy version
   ```
6. server: download video files (~3.5GB)

   we will use [Big Buck Bunny](https://peach.blender.org/), an
   open-source film.

   for DASH, we need separate audio and video segments located at
   <https://dash.akamaized.net/akamai/bbb_30fps/>:
   ```shell
   mkdir -p video-streaming/www/data/bbb
   cd video-streaming/www/data/bbb
   wget -r -N --no-parent -nH https://dash.akamaized.net/akamai/bbb_30fps/bbb_30fps_1920x1080_8000k
   wget -r -N --no-parent -nH https://dash.akamaized.net/akamai/bbb_30fps/bbb_30fps_1280x720_4000k
   wget -r -N --no-parent -nH https://dash.akamaized.net/akamai/bbb_30fps/bbb_30fps_1024x576_2500k
   wget -r -N --no-parent -nH https://dash.akamaized.net/akamai/bbb_30fps/bbb_a64k
   wget https://dash.akamaized.net/akamai/bbb_30fps/bbb_30fps_1920x1080_8000k.mpd
   wget https://dash.akamaized.net/akamai/bbb_30fps/bbb_30fps_1280x720_4000k.mpd
   wget https://dash.akamaized.net/akamai/bbb_30fps/bbb_30fps_1024x576_2500k.mpd
   wget https://dash.akamaized.net/akamai/bbb_30fps/bbb_a64k.mpd
   ```

   for HTML5 and HLS, we need to grab the full video files (HTML5) and
   segment them ourselves (HLS) using [Bento4](https://www.bento4.com/downloads/):
   ```shell
   mkdir -p video-streaming/www/data/bbb
   cd video-streaming/www/data/bbb
   wget https://dash.akamaized.net/akamai/bbb_30fps/bbb_30fps_1920x1080_8000k.mp4
   wget https://dash.akamaized.net/akamai/bbb_30fps/bbb_30fps_1280x720_4000k.mp4
   wget https://dash.akamaized.net/akamai/bbb_30fps/bbb_30fps_1024x576_2500k.mp4
   bash video-streaming/scripts/bento.sh
   ```
7. server: run the web server
   ```shell
   bash video-streaming/www/run.sh
   ```

   you can test audio/video playback by opening a web browser to
   <http://127.0.0.1:8001>.

8. client: run the client automation
   ```shell
   bash video-streaming/client/run.sh --server 127.0.0.1:8001 --protocol http --resolution 1080 --time 1
   ```

## server

## client

## scripts

- `bento.sh`: segments an .mp4 file to .ts files for HLS
- `install-gui.sh`: bootstraps a fresh Ubuntu install with the GNOME desktop and Google Chrome

## libraries used

| name                                     | link                                             | license        |
| ---                                      | ---                                              | ---            |
| dash.js                                  | https://github.com/Dash-Industry-Forum/dash.js   | `BSD-3-Clause` |
| hls.js                                   | https://github.com/video-dev/hls.js              | `Apache-2.0`   |
| Plyr                                     | https://github.com/sampotts/plyr                 | `MIT`          |
| Playwright                               | https://github.com/microsoft/playwright          | `Apache-2.0`   |
| small-n-flat icon pack (repository icon) | https://www.iconfinder.com/iconsets/small-n-flat | `CC-BY-3.0`    |

## copyright

Copyright (C) 2020–2022  University of Southern California

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## license

[`GPL-3.0-or-later`](./LICENSE)
