#!/bin/bash

# change into the directory containing this script
cd "$(dirname "${BASH_SOURCE[0]}")"

# pause 5s for the webserver to load properly
#
# we won't check for aliveness using wget/curl to avoid polluting the
# pcap files with GET requests
sleep 3

# PLAYWRIGHT_BROWSERS_PATH - specify a shared folder that playwright
# will use to download browsers and to look for browsers when launching
# browser instances.
export PLAYWRIGHT_BROWSERS_PATH=0

# manually set the DISPLAY variable so that it shows up on the already
# logged-in user
export DISPLAY=:0.0

# set defaults in case some or no parameters are passed
server="10.0.7.100:8001"
protocol="http"
resolution="1080"
time="1"

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --server)     server="$2"; shift ;;
        --protocol)   protocol="$2"; shift ;;
        --resolution) resolution="$2"; shift ;;
        --time)       time="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

# watch-video.js usage: node watch-video.js [IP_ADDRESS:PORT] [PROTOCOL] [RESOLUTION] [TIME_TO_PLAY]
#
# node watch-video.js 10.0.7.100:8001 hls 1080 1
node watch-video.js $server $protocol $resolution $time
