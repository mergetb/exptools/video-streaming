// example execution:
//   DISPLAY=:0.0 node script-name.js 10.0.7.100:8001
const { chromium } = require('playwright');

(async () => {

    async function closeAndExit(browser) {
        console.log("closeAndExit called");
        await browser.close();
        process.exit(0);
    }

    // default arguments for chromium are here:
    // https://github.com/microsoft/playwright/blob/75edc61531b44fa657157b8e8481fcc8bc440abf/src/server/chromium/chromium.ts
    const browser = await chromium.launch({
        headless: false,
        viewport: null,
        executablePath: "/usr/bin/google-chrome",
    });
    const page = await browser.newPage();

    // get website address from parameter passed to this script
    // node script-name.js arg0 arg1 .. argN
    //
    // node watch-video.js [ip:port] [protocol] [resolution := {576, 720, 1080}] [playbackTime := minutes]
    var args = process.argv.slice(2);
    if (args.length == 0) {
        console.log("no arguments, using defaults 10.0.7.100:8001 http 576 1");
        serverAddr        = "10.0.7.100:8001";
        streamingProtocol = "http";
        resolution        = "576";
        playbackTime      = 1;
    } else {
        // check for expected number of arguments!
        if (args.length == 4) {
            serverAddr        = args[0];
            streamingProtocol = args[1];
            resolution        = args[2];
            playbackTime      = args[3];
            console.log(`arguments: ${serverAddr} ${streamingProtocol} ${resolution} ${playbackTime}`);
        } else {
            console.error("expected 4 arguments, received " + args.length + "!");
            await closeAndExit(browser);
        }
    }

    //await closeAndExit(browser); // for debugging
    //
    let endpoint = "";
    if (streamingProtocol == "http") {
        endpoint = "index.html";
    } else if (streamingProtocol == "dash") {
        endpoint = "dash.html";
    } else if (streamingProtocol == "hls") {
        endpoint = "hls.html";
    } else {
        console.error(`protocol ${streamingProtocol} not supported in {http, dash, hls}. exiting.`);
        await closeAndExit(browser);
    }

    await page.goto('http://' + serverAddr + '/' + endpoint);
    await page.waitForTimeout(2000);

    // change quality settings based on parameters
    if (streamingProtocol == "http") {
        // choose quality setting
        //
        // note that playwright expects elements to be visible, so we need to
        // mimic what a user would click to change the quality settings (or change
        // the defaults on the website).
        //
        await page.click('button[data-plyr="settings"]');
        await page.waitForTimeout(200);

        await page.click('button[class~="plyr__control--forward"][role="menuitem"]:nth-child(2)');
        await page.waitForTimeout(2000);

        // with plyr.js, changing the quality will autoplay afterwards.
        if (resolution == "1080") {
            await page.click('button[data-plyr="quality"][value="1080"]', {force: true })
        } else if (resolution == "720") {
            await page.click('button[data-plyr="quality"][value="720"]', {force: true })
        } else if (resolution == "576") {
            await page.click('button[data-plyr="quality"][value="576"]', {force: true })
        } else {
            // default is 1080
            await page.click('button[data-plyr="quality"][value="1080"]', {force: true })
        }

        /*
        // close settings page
        await page.click('button[data-plyr="settings"]');
        await page.waitForTimeout(500);

        // play video
        console.log("pressing play");
        await page.click('button[data-plyr="play"]', {timeout: 500});
        //*/
    } else if (streamingProtocol == "dash") {
        await page.click('div[id="bitrateListBtn"]');
        await page.waitForTimeout(200);

        // video-bitrate-listItem_0 = auto
        // video-bitrate-listItem_1 = 576p
        // video-bitrate-listItem_2 = 720p
        // video-bitrate-listItem_3 = 1080p 
        if (resolution == "1080") {
            await page.click('li[id="video-bitrate-listItem_3"]', {force: true })
        } else if (resolution == "720") {
            await page.click('li[id="video-bitrate-listItem_2"]', {force: true })
        } else if (resolution == "576") {
            await page.click('li[id="video-bitrate-listItem_1"]', {force: true })
        } else {
            // default is auto
            await page.click('li[id="video-bitrate-listItem_0"]', {force: true })
        }

        // play video
        await page.waitForTimeout(500);
        console.log("pressing play");
        await page.click('div[id="playPauseBtn"]', {timeout: 500});

    } else if (streamingProtocol == "hls") {
        if (resolution == "1080") {
            await page.click('button[id="2"]', {force: true })
        } else if (resolution == "720") {
            await page.click('button[id="1"]', {force: true })
        } else if (resolution == "576") {
            await page.click('button[id="0"]', {force: true })
        } else {
            // default is auto
            await page.click('button[id="-1"]', {force: true })
        }
    } else {
        console.error("Not implemented yet!");
        closeAndExit(browser);
    }

    // escape focus by clicking on a noop element
    await page.waitForTimeout(500);
    await page.click('div[id="click"]');

    // set time to play (or time browser is open) based on `playbackTime`. if
    // you don't run `browser.close()` at some point, the VM will possibly
    // crash.
    var timeToLive = playbackTime * 60 * 1000;
    console.log(`waiting for timeout ${timeToLive} ms`);

    await page.waitForTimeout(timeToLive);

    console.log("timeout reached, closing browser");
    await browser.close();
})();
