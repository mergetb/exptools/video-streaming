#!/bin/bash
#
# install-gui.sh
#
# Description: run this script on a virtual machine or sandboxed
# environment running Ubuntu to automatically install a working desktop
# environment with Google Chrome.
#

###
# configuration variables
#
AUTOMATIC_LOGIN=0   # set to 1 if you want an automatic login
USERNAME=foobar     # username you want to login automatically as

###
# script should run as root
#
if [ ${EUID} -ne 0 ]; then
    echo "Must be run as root"
    exit 1
fi

###
# protect /etc/resolv.conf from being overwritten by Network Manager
#
echo "adding +i attribute to /etc/resolv.conf"
chattr +i /etc/resolv.conf

###
# install packages
#
echo "downloading google chrome"
curl -L https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb > /tmp/google-chrome-stable_current_amd64.deb

echo "installing ubuntu-gnome-desktop + codecs + chrome...this will take a while"
apt-get update
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections
apt-get install -y ubuntu-gnome-desktop ubuntu-restricted-extras /tmp/google-chrome-stable_current_amd64.deb

###
# enable automatic login
#
if [ ${AUTOMATIC_LOGIN} -eq 1 ]; then
echo "enabling automatic login to gdm"
cat <<EOF >/etc/gdm3/custom.conf
[daemon]
AutomaticLoginEnable = true
AutomaticLogin = ${USERNAME}
EOF
fi

###
# disable screen locking and screensavers
#
echo "disabling screensavers and locks"
cat << EOF > /usr/share/glib-2.0/schemas/90_ubuntu-settings.gschema.override
[org.gnome.desktop.screensaver]
lock-enabled = false
[org.gnome.desktop.session]
idle-delay = 0
[org.gnome.desktop.lockdown]
disable-lock-screen = true
[org.gnome.desktop.screensaver]
idle-activation-enabled = false
[org.gnome.settings-daemon.plugins.power]
idle-dim = false
EOF

glib-compile-schemas /usr/share/glib-2.0/schemas/

###
# disable "first login" GNOME prompts
#
echo "disabling GNOME first-login"
echo "X-GNOME-Autostart-enabled=false" >> /etc/xdg/autostart/gnome-initial-setup-first-login.desktop

###
# disable GNOME update-notifier
#
echo "disabling GNOME update-notifier"
sed -i 's/X-GNOME-Autostart-Delay=60/X-GNOME-Autostart-enabled=false/' /etc/xdg/autostart/update-notifier.desktop

###
# disable Network Manager
#
echo "disabling Network Manager"
systemctl stop NetworkManager.service
systemctl stop NetworkManager-wait-online.service
systemctl stop NetworkManager-dispatcher.service
systemctl stop network-manager.service

systemctl disable NetworkManager.service
systemctl disable NetworkManager-wait-online.service
systemctl disable NetworkManager-dispatcher.service
systemctl disable network-manager.service

systemctl mask NetworkManager.service
systemctl mask NetworkManager-wait-online.service
systemctl mask NetworkManager-dispatcher.service
systemctl mask network-manager.service

###
# finally, start GNOME Display Manager (gdm)
#
echo "starting GNOME"
systemctl start gdm.service

echo "done!"
