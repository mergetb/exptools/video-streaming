# HTTP Live Streaming (HLS)
HTTP Live Streaming (also known as HLS) is an HTTP-based adaptive bitrate streaming communications protocol developed by Apple Inc. and released in 2009. 
 * Note: HLS support appears spotty in desktop browsers.

HLS Overview: <https://developer.apple.com/documentation/http_live_streaming>
Example Streams: <https://developer.apple.com/streaming/examples/>


