#!/bin/bash

CADDY="caddy"

# change into the directory containing this script
cd "$(dirname "${BASH_SOURCE[0]}")"

$CADDY \
    run \
    --config ./Caddyfile \
    --watch
